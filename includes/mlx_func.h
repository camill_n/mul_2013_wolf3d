/*
** mlx_func.h for fdf in /home/camill_n/rendu/MUL_2013_fdf
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Wed Nov 27 23:17:49 2013 Nicolas Camilli
** Last update Fri Dec 20 17:43:10 2013 Nicolas Camilli
*/

#ifndef MLX_FUNC_H_
# define MLX_FUNC_H_
# define WIDTH 1920
# define HEIGHT 1080
# define N "Wold3D"
# define ANG_R 0
# define ANG_L 1
# define UP 1
# define DOWN 2
# define RIGHT 3
# define LEFT 4

typedef struct	s_mlx
{
  void		*mlx_ptr;
  void		*win_ptr;
}		t_mlx;

typedef struct	s_pic
{
  int		bpp;
  int		size;
  int		endian;
  char		*content;
  void		*img_ptr;
  int		color;
}		t_pic;

typedef struct	s_color
{
  int		r;
  int		v;
  int		b;
  int		t;
}		t_color;

typedef struct	s_coords
{
  int		x_1;
  int		y_1;
  int		x_2;
  int		y_2;
}		t_coords;

typedef	struct	s_map
{
  float		cte1;
  float		cte2;
  int		couleur;
  int		**map_content;
  int		width;
  int		height;
  int		size_line;
  int		mode;
}		t_map;

typedef struct	s_global
{
  t_mlx		*mlx;
  t_pic		*pic;
  t_map		*map;
  float		X0;
  float		Y0;
  int		A;
  int		quadri;
  float		cos[360];
  float		sin[360];
  int		minimap;
  int		mode_l;
}		t_global;

int		gere_key(int keycode, t_global *global);
int		gere_expose(t_global *global);
int		gere_mouse(int button, int x, int y, t_global *global);
void		error_malloc(char *str);
void		error_arg();
int		aff_ligne(t_global *global, t_coords coords, t_color color);
void		error_matrice(int i);
void		error_too_large(int i);
void		set_mlx_params(t_global *global);
void		put_pixel(t_global *global, int x, int y, t_color col);
void		set_map(char **av, t_global *global);
t_coords	set_coords(int x_1, int y_1, int x_2, int y_2);
t_color		set_color(int r, int v, int b, int t);
int		print_mat(t_map *map);
void		display_minimap(t_global *global);
void		draw_circle(t_global *global, t_coords coords, t_color col, int rayon);
void		map_conf(t_global *global);
void		display_map(t_global *global);
int		gere_editor(t_global *global, int x, int y);

#endif
