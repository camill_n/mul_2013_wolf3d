/*
** my_putchar.c for my_putchar in /home/camill_n/rendu/Piscine-C-lib/my
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Tue Oct  8 07:18:20 2013 Nicolas Camilli
** Last update Fri Nov 15 14:25:54 2013 Nicolas Camilli
*/

#include "../includes/func.h"

void	manage_putchar(va_list ap)
{
  my_putchar((char)va_arg(ap, int));
}

void	my_putchar(char c)
{
  write(1, &c, 1);
}
