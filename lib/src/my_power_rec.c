/*
** my_power_rec.c for my_power_rec in /home/camill_n/rendu/Piscine-C-Jour_05
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Fri Oct  4 14:52:49 2013 Nicolas Camilli
** Last update Fri Nov 29 13:53:42 2013 Nicolas Camilli
*/

int	my_power_rec(int nb, int power)
{
  if (power == 0)
    return (1);
  if (power != 1)
    nb = nb * my_power_rec(nb, power - 1);
  else
    return (nb);
}
