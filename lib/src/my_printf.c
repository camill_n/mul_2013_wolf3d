/*
** my_printf.c for my_printf in /home/camill_n/rendu/PSU_2018_my_printf2
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Thu Nov 14 14:45:46 2013 Nicolas Camilli
** Last update Sun Nov 17 10:15:33 2013 Nicolas Camilli
*/

#include "../includes/func.h"

int		my_printf(const char *str, ...)
{
  va_list	ap;
  t_func	*tab;
  int		i;

  i = 0;
  set_func(&tab);
  va_start(ap, str);
  while (str[i] != '\0')
    {
      if (str[i] == '%' && str[i + 1] != '\0')
	{
	  if (get_id_func(str, &i, &tab) != -1)
	    (tab[get_id_func(str, &i, &tab)].func_tab)(ap);
	  else
	    my_putchar(str[i]);
	  if (str[i + 1] == 'l' && str[i + 2] == 'd')
	    i++;
	  i++;
	}
      else
	my_putchar(str[i]);
      i++;
    }
  free(tab);
  va_end(ap);
}
