/*
** my_getnbr.c for my_getnbr.c in /home/camill_n/rendu/Piscine-C-lib/my
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Tue Oct  8 11:40:54 2013 Nicolas Camilli
** Last update Fri Nov 29 13:31:16 2013 Nicolas Camilli
*/

void	ini_var(int *i, int *pow, int *result)
{
  *result = 0;
  *pow = 0;
  *i = 0;
}

void	verif_neg(int *result, int start, char *str)
{
  if (str[start - 1] == '-')
    {
      *result = *result * -1;
    }
}

int	my_getnbr(char *str)
{
  int	i;
  int	start;
  int	pow;
  int	result;

  ini_var(&i, &pow, &result);
  while (str[i] != '\0' && str[i] == '-' || str[i] == '+')
    {
      i = i + 1;
    }
  start = i;
  while (str[i] != '\0' && str[i] >= '0' && str[i] <= '9')
    {
      i = i + 1;
    }
  i = i - 1;
  while (i >= start)
    {
      result = result + (str[i] - 48) * (my_power_rec(10, pow));
      pow = pow + 1;
      i = i - 1;
    }
  verif_neg(&result, start, str);
  return (result);
}
