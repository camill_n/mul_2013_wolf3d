/*
** get_next_line.h for get_next_line in /home/camill_n/rendu/get_next_l-camill_n
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Tue Nov 12 19:55:13 2013 Nicolas Camilli
** Last update Thu Nov 28 16:11:17 2013 Nicolas Camilli
*/

#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_

# define SIZE_READ 1024

int	my_strlen(char *str);
char	*my_strncat(char *src, int i, int j);
char	*my_str_add(char *dst, char *src);
char	*check_save(char **buffer_save, char *buffer_s_copy);
char	*get_next_line(const int fd);

#endif /* !GET_NEXT_LINE_H_ */
