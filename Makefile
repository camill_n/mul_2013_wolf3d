##
## Makefile for test in /home/camill_n/rendu/Piscine-C-lib/my
##
## Made by Nicolas Camilli
## Login   <camill_n@epitech.net>
##
## Started on  Mon Oct 21 07:17:19 2013 Nicolas Camilli
## Last update Sat Dec 21 15:49:07 2013 Nicolas Camilli
##

SRCS  	= main.c \
	  src/errors.c \
	  src/display.c \
	  src/init.c \
	  src/vector.c \
	  src/hook.c \
	  src/minimap.c \
	  src/expose.c \
	  src/get_next_line.c \
	  src/map.c \

SRCS_LIB = lib/src/my_printf.c \
	   lib/src/my_putchar.c \
	   lib/src/my_putstr.c \
	   lib/src/my_getnbr.c \
	   lib/src/my_power_rec.c \
	   lib/src/my_put_bin.c \
	   lib/src/my_getnbr.c \
	   lib/src/my_put_hexa.c \
	   lib/src/change_base.c \
	   lib/src/my_put_nbr.c \
	   lib/src/func_tab.c \
	   lib/src/my_revstr.c \

RM	= rm -f

NAME	= wolf3d

AR	= ar rc

CC	= cc -O6

LIB_PATH = lib/

OBJS	= $(SRCS:.c=.o)

OBJS_LIB = $(SRCS_LIB:.c=.o)

LIB = libmy

CFLAGS = -I./includes

all: 	$(LIB) $(NAME)

.c.o:
	$(CC) $(CFLAGS) -c $< -o $(<:.c=.o)

$(LIB): $(OBJS_LIB)
	$(AR) includes/libmy.a lib/src/*.o
	ranlib includes/libmy.a

$(NAME): $(OBJS)
	 $(CC) $(OBJS) -o $(NAME) -L./includes -lmlx -lXext -lX11 -lmy -L/usr/lib64 -lm

clean:
	$(RM) $(OBJS) $(OBJS_LIB)

fclean:	clean
	$(RM) $(NAME) libmy.a

re: fclean all
