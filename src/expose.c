/*
** expose.c for fdf in /home/camill_n/rendu/MUL_2013_fdf
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Thu Nov 28 00:43:41 2013 Nicolas Camilli
** Last update Fri Dec 20 17:45:28 2013 Nicolas Camilli
*/

#include <stdlib.h>
#include "mlx.h"
#include "mlx_func.h"

void	empty_pic(t_pic *pic)
{
  int	i;

  i = 0;
  while (i < (HEIGHT * WIDTH * (pic->bpp / 8)))
    pic->content[i++] = 0x00;
}

int	gere_key(int keycode, t_global *global)
{
  keycode == 65361 ? gere_angle(global, ANG_L) : 0;
  keycode == 65363 ? gere_angle(global, ANG_R) : 0;
  keycode == 122 ? gere_move(global, UP) : 0;
  keycode == 115 ? gere_move(global, DOWN) : 0;
  keycode == 100 ? gere_move(global, RIGHT) : 0;
  keycode == 113 ? gere_move(global, LEFT) : 0;
  keycode == 65438 ? global->minimap *= -1 : 0;
  keycode == 65436 ? global->quadri *= -1 : 0;
  keycode == 65451 ? global->mode_l = 0 : 0;
  keycode == 65453 ? global->mode_l = 1 : 0;
  if (keycode == 65307)
    exit(0);
  gere_expose(global);
}

int	gere_mouse(int button, int x, int y, t_global *global)
{
  gere_editor(global, x, y);
  gere_expose(global);
}

int	gere_expose(t_global *global)
{
  empty_pic(global->pic);
  if (global->minimap == 1)
    display_minimap(global);
  display_map(global);
  mlx_put_image_to_window(global->mlx->mlx_ptr, global->mlx->win_ptr,
                          global->pic->img_ptr, 0, 0);
}
