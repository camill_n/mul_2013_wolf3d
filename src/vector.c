/*
** vector.c for fdf in /home/camill_n/rendu/MUL_2013_fdf
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Thu Nov 28 00:54:09 2013 Nicolas Camilli
** Last update Wed Dec 18 15:52:31 2013 Nicolas Camilli
*/

#include <math.h>
#include "mlx_func.h"

int	gere_cas(t_coords *coords)
{
  int	tmp;

  if (coords->x_2 - coords->x_1 < 0)
    {
      tmp = coords->x_1;
      coords->x_1 = coords->x_2;
      coords->x_2 = tmp;
      tmp = coords->y_1;
      coords->y_1 = coords->y_2;
      coords->y_2 = tmp;
    }
}

int	aff_ligne(t_global *global, t_coords coords, t_color color)
{
  int	dx;
  int	dy;
  int	i;
  int	xinc;
  int	yinc;
  int	cumul;
  int	x;
  int	y;

  x = coords.x_1;
  y =  coords.y_1;
  dx = coords.x_2 - coords.x_1;
  dy = coords.y_2 - coords.y_1;
  xinc = (dx > 0) ? 1 : -1;
  yinc = (dy > 0) ? 1 : -1;
  dx = abs(dx);
  dy = abs(dy);
  put_pixel(global, x, y, color);
  i = 1;
  if (dx > dy)
    {
      cumul = dx / 2 ;
      while (i <= dx)
	{
	  x += xinc;
	  cumul += dy;
	  if (cumul >= dx)
	    {
	      cumul -= dx;
	      y += yinc;
	    }
	  put_pixel(global, x, y, color);
	  ++i;
	}
    }
  else
    {
      cumul = dy / 2 ;
      while (i <= dy)
	{
	  y += yinc;
	  cumul += dx;
	  if (cumul >= dy)
	    {
	      cumul -= dy;
	      x += xinc;
	    }
	  put_pixel(global, x, y, color);
	  ++i;
	}
    }
}

t_color		set_color(int r, int v, int b, int t)
{
  t_color	color;

  color.r = r;
  color.v = v;
  color.b = b;
  color.t = t;
  return (color);
}

void	put_pixel(t_global *global, int x, int y, t_color col)
{
  int	i;

  i = y * (WIDTH * (global->pic->bpp / 8));
  i += x * (global->pic->bpp / 8);
  global->pic->content[i++] = mlx_get_color_value(global->mlx->mlx_ptr, col.r);
  global->pic->content[i++] = mlx_get_color_value(global->mlx->mlx_ptr, col.v);
  global->pic->content[i++] = mlx_get_color_value(global->mlx->mlx_ptr, col.b);
  global->pic->content[i] = mlx_get_color_value(global->mlx->mlx_ptr, col.t);
}

#include <unistd.h>
void	draw_circle(t_global *global, t_coords coords, t_color col, int rayon)
{
  int	i;

  i = 0;
  while (i < 360)
    {
      put_pixel(global, coords.x_1 + (rayon * cos(i)),
		coords.y_1 + (rayon * sin(i)), col);
      ++i;
    }
}
