/*
** errors.c for fdf in /home/camill_n/rendu/MUL_2013_fdf/src
** 
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
** 
** Started on  Wed Nov 27 23:29:38 2013 Nicolas Camilli
** Last update Tue Dec 17 14:19:34 2013 Nicolas Camilli
*/

#include <stdlib.h>

void	error_malloc(char *str)
{
  my_printf("Memory acces denied for var: %s\n", str);
  exit(0);
}

void	error_path(char *str)
{
  my_printf("wold3D cannot access: %s\n", str);
  exit(0);
}

void	error_arg()
{
  my_printf("Usage: ./wold3D [map_file] and require environement variables\n");
  exit(0);
}

void	error_matrice(int i)
{
  my_printf("Mistake in matrice file: line: %d\n", i);
  exit(0);
}

void	error_too_large(int i)
{
  my_printf("Number too small or too large: line: %d\n", i);
  exit(0);
}
