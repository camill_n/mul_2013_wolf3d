/*
** minimap.c for wolf3d in /home/camill_n/rendu/MUL_2013_wolf3d
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Mon Dec 16 23:22:51 2013 Nicolas Camilli
** Last update Fri Dec 20 17:43:48 2013 Nicolas Camilli
*/

#include <math.h>
#include "mlx.h"
#include "mlx_func.h"

void	draw_rec(t_global *global, t_coords coords, t_color col)
{
  int	tmp;

  while (coords.y_1 < coords.y_2)
    {
      tmp = coords.x_1;
      while (tmp < coords.x_2)
	{
	  put_pixel(global, tmp, coords.y_1, col);
	  ++tmp;
	}
      ++coords.y_1;
    }
}

void   color_minimap(t_global *global)
{
  int	i;
  int	j;
  int	X;
  int	Y;

  i = 0;
  while (i < global->map->height)
    {
      j = 0;
      while (j < global->map->width)
	{
	  X = (j * (490 / global->map->width)) + 1450;
	  Y = (i * (200 / global->map->height));
	  if (global->map->map_content[i][j] == 0)
	    draw_rec(global, set_coords(X, Y, X + (490 / global->map->width),
					Y + (200 / global->map->height)),
		     set_color(0x09, 0x20, 0x3b, 0x00));
	  else
	    draw_rec(global, set_coords(X, Y, X + (490 / global->map->width),
					Y + (200 / global->map->height)),
		     set_color(0x79 + global->map->map_content[i][j] * 30,
			       0x71 + global->map->map_content[i][j] * 30,
			       0x67 + global->map->map_content[i][j] * 30,
			       0x00));
	  ++j;
	}
      ++i;
    }
}

void	place_player(t_global *global)
{
  int	X;
  int	Y;

  X = (global->X0 * (490 / global->map->width)) + 1450;
  X += (490 / global->map->width) / 2;
  Y = (global->Y0 * (200 / global->map->height));
  Y += (200 / global->map->height) / 2;
  draw_circle(global, set_coords(X, Y, 0, 0),
	      set_color(0xff, 0xff, 0xff, 0x00), 5);
}

void	display_minimap(t_global *global)
{
  int	i;
  int	j;
  int	X;
  int	Y;

  i = 0;
  color_minimap(global);
  while (i < global->map->height + 1 && global->quadri == 1)
    {
      j = 0;
      while (j < global->map->width + 1)
	{
	  X = (j * (490 / global->map->width)) + 1450;
	  Y = (i * (200 / global->map->height));
	  if (j != global->map->width)
	    aff_ligne(global, set_coords(X, Y, X + (490 / global->map->width), Y),
		      set_color(0xc3, 0x9a, 0x9a, 0x00));
	  if (i != global->map->height)
	    aff_ligne(global, set_coords(X, Y, X , Y + (200 / global->map->height)),
		      set_color(0xc3, 0x9a, 0x9a, 0x00));
	  ++j;
	}
      ++i;
    }
  place_player(global);
}
