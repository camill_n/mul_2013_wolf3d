/*
** display.c for wold3D in /home/camill_n/rendu/MUL_2013_wolf3d
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Wed Dec 18 15:47:05 2013 Nicolas Camilli
** Last update Fri Dec 20 17:37:08 2013 Nicolas Camilli
*/

#include <stdlib.h>
#include <stdio.h>
#include "mlx_func.h"
#include "mlx.h"


void	display_proj(t_global *global, float X_2, float Y_2, float P)
{
  float	X;
  float	Y;
  float	n_x;
  float	n_y;

  X = (global->X0 * (490 / global->map->width)) + 1450;
  X += (490 / global->map->width) / 2;
  Y = (global->Y0 * (200 / global->map->height));
  Y += (200 / global->map->height) / 2;
  n_x = (X_2 * (490 / global->map->width)) + 1450;
  n_y = (Y_2 * (200 / global->map->height));
  X_2 < 0 ? X_2 = 0 : 0;
  Y_2 > WIDTH ? Y_2 = WIDTH : 0;
  aff_ligne(global, set_coords((int)X, (int)Y, (int)n_x, (int)n_y),
	    set_color(0x30, 0x7e, 0xe6, 0xcc));
}

int	dist_wall(t_global *global, int x, float P)
{
  float	vec_y;
  float	XP;
  float	YP;
  float	D;

  D = P / 2;
  vec_y = (P * ((WIDTH / 2) - x) / WIDTH);
  XP = (D * global->cos[global->A] - (vec_y * global->sin[global->A]));
  YP = (D * global->sin[global->A]) + (vec_y * global->cos[global->A]);
  XP += global->X0;
  YP += global->Y0;
  (int)XP > global->map->width ? XP = global->map->width : 0;
  (int)YP > global->map->height ? YP = global->map->height : 0;
  (int)XP < 0 ? XP = 0 : 0;
  (int)YP < 0 ? YP = 0 : 0;
  if (XP == 0 || YP == 0 || XP == global->map->width ||
      YP == global->map->height ||
      global->map->map_content[(int)YP][(int)XP] > 0)
    {
      if (global->minimap == 1)
	display_proj(global, XP, YP, P);
      return (1);
    }
  return (0);
}

void		sky(t_global *global, int i, int size_p)
{
  int		j;
  t_color	color;

  j = 0;
  if (global->mode_l == 0)
    {
      color = set_color(0xd0, 0xd0, 0x3a, 0x00);
      while (j < (HEIGHT / 2) - (size_p / 2))
	{	
	  j % 2 == 0 ? put_pixel(global, i, j++, color) : 0;
	  j % 2 == 1 ? put_pixel(global, i, j++, set_color(0xff, 0xff, 0xff, 0xff)) : 0;
	}
    }
  else
    aff_ligne(global, set_coords(i, 0,
				 i, (HEIGHT / 2) - (size_p / 2)),
	      set_color(0x4d, 0x15, 0x03, 0x00));
}

void	sol(t_global *global, int i, int size_p)
{
  if (global->mode_l == 0)
    aff_ligne(global, set_coords(i, (HEIGHT / 2) + (size_p / 2),
				 i, HEIGHT),
	      set_color(0x09, 0x20, 0x3b, 0x00));
  else
    aff_ligne(global, set_coords(i, (HEIGHT / 2) + (size_p / 2),
				 i, HEIGHT),
	      set_color(0x05, 0x05, 0x00, 0x00));
}

void	display_map(t_global *global)
{
  int	i;
  float	P;
  float	size_p;

  i = 0;
  while (i < 1450)
    {
      P = 1;
      while (dist_wall(global, i, P) == 0)
	P += 0.008;
      size_p = (HEIGHT / (2 * P)) * 2;
      if (global->mode_l == 0)
	aff_ligne(global, set_coords(i, (HEIGHT / 2) - (size_p / 2),
				     i, (HEIGHT / 2) + (size_p / 2)),
		  set_color(0x79 - P * 2.5, 0x71 - P * 2.5, 0x67 - P * 2.5, 0x00));
      else
	aff_ligne(global, set_coords(i, (HEIGHT / 2) - (size_p / 2),
				     i, (HEIGHT / 2) + (size_p / 2)),
		  set_color(0x02, 0x03, 0x00, 0x00));
      sky(global, i, size_p);
      sol(global, i, size_p);
      ++i;
    }
}
