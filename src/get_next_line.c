/*
** get_next_line.c for get_next_line in /home/camill_n/rendu/get_next_line-2018-camill_n
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Thu Nov 21 08:08:02 2013 Nicolas Camilli
** Last update Fri Nov 29 13:21:08 2013 Nicolas Camilli
*/

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "../includes/get_next_line.h"

int		my_strlen(char *str)
{
  int		i;

  i = 0;
  while (str[i] != '\0')
    i++;
  return (i);
}

char		*my_strncat(char *src, int i, int j)
{
  int		k;
  char		*new_str;

  k = 0;
  new_str = malloc((my_strlen(src) + 1) * sizeof(char));
  if (new_str != NULL)
    {
      while (i < j)
	new_str[k++] = src[i++];
      new_str[k] = '\0';
      return (new_str);
    }
  else
    return (NULL);
}

char		*my_str_add(char *dst, char *src)
{
  char		*new_str;
  int		i;
  int		j;

  new_str = malloc((my_strlen(dst) + my_strlen(src) + 1) * sizeof(char));
  if (new_str != NULL)
    {
      i = 0;
      j = 0;
      while (dst[i] != '\0')
	new_str[j++] = dst[i++];
      i = 0;
      while (src[i] != '\0')
	new_str[j++] = src[i++];
      new_str[j] = '\0';
      return (new_str);
    }
  else
    return (NULL);
}

char		*check_save(char **buffer_save, char *buffer_s_copy)
{
  int		i;
  char		*tmp;

  i = 0;
  while (buffer_s_copy[i] != '\0')
    {
      if (buffer_s_copy[i] == '\n')
	{
	  tmp = my_strncat(buffer_s_copy, 0, i);
	  *buffer_save += (i + 1);
	  return (tmp);
	}
      i = i + 1;
    }
  return (NULL);
}

char		*get_next_line(const int fd)
{
  static char	*buffer_save = "";
  char		current_buff[SIZE_READ];
  int		ret;
  char		*tmp;
  int		end;

  end = 0;
  if (tmp = check_save(&buffer_save, buffer_save))
    return (tmp);
  while (ret = read(fd, current_buff, SIZE_READ))
    {
      current_buff[ret] = '\0';
      buffer_save = my_str_add(buffer_save, current_buff);
      if (tmp = check_save(&buffer_save, buffer_save))
	return (tmp);
    }
  if (my_strlen(buffer_save) > 0 && end++ == 0)
    return (buffer_save);
  return (NULL);
}
