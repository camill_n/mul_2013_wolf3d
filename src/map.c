/*
** map.c for fdf in /home/camill_n/rendu/MUL_2013_fdf/src
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Thu Nov 28 15:01:46 2013 Nicolas Camilli
** Last update Mon Dec 16 23:08:05 2013 Nicolas Camilli
*/

#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "mlx_func.h"
#include "get_next_line.h"

int	count_height(char *path)
{
  int	height;
  int	fd;
  char	*s;

  height = 0;
  fd = open(path, O_RDONLY);
  if (fd != -1)
    {
      while (s = get_next_line(fd))
	{
	  my_printf("");
	  height++;
	  free(s);
	}
      close(fd);
    }
  else
    error_path(path);
  return (height);
}


void	width_line(char *s, int *width)
{
  int	i;
  int	j;
  int	width_t;

  i = 0;
  width_t = 0;
  while (s[i] != '\0')
    {
      if (s[i] >= '0' && s[i] <= '9')
	{
	  j = i;
	  while (s[j] >= '0' && s[j] <= '9' && s[j] != '\0')
	    j++;
	  i = j;
	  width_t++;
	}
      i++;
    }
  *width = width_t;
}

void	set_line(char *s, t_map *map, int k, int *width)
{
  int	i;
  int	j;
  char	*tmp;
  int	t;

  t = 0;
  i = 0;
  width_line(s, width);
  map->map_content[k] = malloc((*width) * sizeof(int));
  while (s[i] != '\0')
    {
      if (s[i] == '-' || (s[i] >= '0' && s[i] <= '9'))
	{
	  j = i;
	  while (s[j] == '-' || (s[j] >= '0' && s[j] <= '9' && s[j] != '\0'))
	    j++;
	  if (map == NULL)
	    error_malloc("map->map_content[k]");
	  tmp = my_strncat(s, i, j);
	  map->map_content[k][t] = my_getnbr(tmp);
	  i = j;
	  t++;
	}
      i++;
    }
}

void	set_map(char **av, t_global *global)
{
  int	height;
  int	width;
  int	verif_w;
  int	fd;
  char	*s;
  int	i;

  global->map->height = count_height(av[1]);
  global->map->map_content = malloc((global->map->height + 1) * sizeof(int *));
  fd = open(av[1], O_RDONLY);
  i = 0;
  width = -1;
  while (s = get_next_line(fd))
    {
      verif_w = width;
      set_line(s, global->map, i, &width);
      if (verif_w != -1 && verif_w != width)
	error_matrice(i + 1);
      free(s);
      i++;
    }
  global->map->width = width;
  close(fd);
}
