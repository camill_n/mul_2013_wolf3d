/*
** hook.c for wolf3d in /home/camill_n/rendu/MUL_2013_wolf3d/src
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Mon Dec 16 23:32:07 2013 Nicolas Camilli
** Last update Fri Dec 20 15:06:12 2013 Nicolas Camilli
*/

#include <stdlib.h>
#include "mlx_func.h"

void	gere_angle(t_global *global, int move)
{
  if (move == ANG_R)
    global->A -= 16;
  if (move == ANG_L)
    global->A += 16;
  if (global->A > 360)
    global->A = global->A - 360;
  if (global->A < 0)
    global->A = global->A + 360;
}

void	init_tmp(t_global *global, int move, float *tmp1, float *tmp2)
{
  *tmp1 = global->X0;
  *tmp2 = global->Y0;
  if (move == UP)
    {
      *tmp1 += global->cos[global->A];
      *tmp2 += global->sin[global->A];
    }
  if (move == DOWN)
    {
      *tmp1 += global->cos[global->A] * -1;
      *tmp2 += global->sin[global->A] * -1;
    }
  if (move == LEFT)
    {
      *tmp1 += global->cos[global->A + 90];
      *tmp2 += global->sin[global->A + 90];
    }
  if (move == RIGHT)
    {
      *tmp1 += global->cos[global->A - 90];
      *tmp2 += global->sin[global->A - 90];
    }
}

void	set_new_map (t_global *global, int ind)
{
  char	*av[3];

  av[0] = NULL;
  av[2] = NULL;
  if (ind == 3)
    av[1] = "bite";
  if (ind == 4)
    av[1] = "map";
  set_map(av, global);
  global->X0 = 2;
  global->Y0 = 2;
}

void	gere_move(t_global *global, int move)
{
  float	tmp1;
  float	tmp2;

  init_tmp(global, move, &tmp1, &tmp2);
  if (tmp1 > 0 && tmp1 < global->map->width && tmp2 > 0 &&
      tmp2 < global->map->height &&
      global->map->map_content[(int)tmp2][(int)tmp1] != 1)
    {
      if (global->map->map_content[(int)tmp2][(int)tmp1] >= 3)
	set_new_map(global, global->map->map_content[(int)tmp2][(int)tmp1]);
      else
	{
	  global->X0 = tmp1;
	  global->Y0 = tmp2;
	}
    }
}

int	gere_editor(t_global *global, int x, int y)
{
  int	X;
  int	Y;

  X = (x - 1450) / (490 / global->map->width);
  Y = y / (200 / global->map->height);
  if (X >= 0 && X <= global->map->width &&
      Y >= 0 && Y <= global->map->height)
    {
      global->map->map_content[Y][X] += 1;
      if (global->map->map_content[Y][X] > 4)
	global->map->map_content[Y][X] = 0;
    }
}
