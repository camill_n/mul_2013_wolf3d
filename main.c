/*
** main.c for my_ls in /home/camill_n/rendu/PSU_2018_my_printf
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Wed Nov 20 01:38:01 2013 Nicolas Camilli
** Last update Fri Dec 20 17:46:26 2013 Nicolas Camilli
*/

#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include "mlx_func.h"
#include "mlx.h"
#include "get_next_line.h"

int		print_mat(t_map *map)
{
  int		i;
  int		j;

  i = 0;
  while (i < (map->height))
    {
      j = 0;
      while (j < (map->width))
	{
	  my_printf("%d ", map->map_content[i][j]);
	  ++j;
	}
      my_putchar('\n');
      i++;
    }
  return (0);
}

int		load_bar()
{
  int		i;
  int		j;
  char		buff[1];
  j = 0;
  i = 0;
  while (i < 10)
    {
      my_putstr("Chargement: [");
      j = 0;
      while (j++ < i + 1)
	my_putstr("#");
      my_putstr("]");
      ++i;
      usleep(700000);
      my_putchar('\r');
    }
  return (0);
}

void		command_display()
{
  char		*s;
  int		fd;

  if (fd = open("COMMAND", O_RDONLY))
    {
      if (fork() == 0)
	{
	  while (s = get_next_line(fd))
	    {
	      write(0, s, my_strlen(s));
	      my_putchar('\n');
	      free(s);
	    }
	  my_putchar('\r');
	  exit(0);
	}
      wait();
    }
  else
    my_printf("No COMMAND file\n");
}

int		main(int ac, char **av)
{
  t_global	global;

  if (ac < 2)
    error_arg();
  command_display();
  if (av[2] == NULL )
    load_bar();
  else
    strcmp(av[2], "-i") != 0 ? load_bar() : 0;
  set_mlx_params(&global);
  set_map(av, &global);
  map_conf(&global);
  if (global.map->width > 1 && global.map->height > 1)
    {
      mlx_key_hook(global.mlx->win_ptr, &gere_key, &global);
      mlx_expose_hook(global.mlx->win_ptr, &gere_expose, &global);
      mlx_mouse_hook(global.mlx->win_ptr, &gere_mouse, &global);
      mlx_loop(global.mlx->mlx_ptr);
    }
  else
    my_printf("Incorrect file or content\n");
  return (0);
}
